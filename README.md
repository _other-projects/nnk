Запуск проекта локально:
1) npm i
2) npm start

Создание билда проекта: npm build

СТРАНИЦЫ ПРОЕКТА.

Формы авторизации:

* Логин: https://_other-projects.gitlab.io/nnk/
* Восстановление пароля: https://_other-projects.gitlab.io/nnk/recovery.html

Страницы клиента:

* Состояние счета: https://_other-projects.gitlab.io/nnk/client/account-state.html
* Информация по карте: https://_other-projects.gitlab.io/nnk/client/account-state/card-info.html
* Распределение средств: https://_other-projects.gitlab.io/nnk/client/account-state/distribution.html
* Счет: https://_other-projects.gitlab.io/nnk/client/account-state/invoice.html

* Выписки: https://_other-projects.gitlab.io/nnk/client/statements.html
* Информация по выписке: https://_other-projects.gitlab.io/nnk/client/statements/details.html
* Прошлые периоды выписок: https://_other-projects.gitlab.io/nnk/client/statements/last-periods.html

* Прайс-лист: https://_other-projects.gitlab.io/nnk/client/price-list.html
* Сообщения: https://_other-projects.gitlab.io/nnk/client/messages.html

Страницы менеджера:

* Клиенты: https://_other-projects.gitlab.io/nnk/manager/clients.html
* Информация по клиенту: https://_other-projects.gitlab.io/nnk/manager/client.html
* История действий: https://_other-projects.gitlab.io/nnk/manager/client/details.html
* 
* Баннеры: https://_other-projects.gitlab.io/nnk/manager/banners.html
* Сообщения: https://_other-projects.gitlab.io/nnk/manager/messages.html
